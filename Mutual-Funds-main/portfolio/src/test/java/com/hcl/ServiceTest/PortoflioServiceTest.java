package com.hcl.ServiceTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.hcl.portfolio.exception.PortfolioNotFoundException;
import com.hcl.portfolio.model.Portfolio;
import com.hcl.portfolio.repository.PortfolioRepository;
import com.hcl.portfolio.service.PortfolioServiceImpl;


@ExtendWith(MockitoExtension.class)
public class PortoflioServiceTest {
	@Mock
	PortfolioRepository portfolioRepository;
	@InjectMocks
	PortfolioServiceImpl portfolioServiceImpl;
	static Portfolio portfolio;

	@Test
	@BeforeAll
	public static void setUp() {
		portfolio = new Portfolio();
		portfolio.setPortfolioId(1);
		portfolio.setName("Keerthi");
		portfolio.setQuantity(1);
		portfolio.setTotalPrice(3000);
	}

	@Test
	@DisplayName("PositiveSenario")
	public void testPortfoliaoId() {
		// context
		when(portfolioRepository.findById(1)).thenReturn(portfolio.get());
		// event
		Optional<Portfolio> result = portfolioServiceImpl.findById(1);
		//Optional<Portfolio> portfolio = portfolioRepository.findById(portfolioId);
		// assertThat(result).isNotNull();
		assertEquals(result.get(), result);
	}

	@Test
	@DisplayName("negativeSenario")
	public void testPortfoliaoId1() throws PortfolioNotFoundException {
		// context
		when(portfolioRepository.findById(0)).thenReturn(PortfolioNotFoundException.class);
		// event
		Optional<Portfolio> result = portfolioServiceImpl.findById(0);
		assertThat(result).isNotNull();
		assertEquals(PortfolioNotFoundException.class, result);

	}

	@Test
	@DisplayName("Find by name:Positive Senario")
	public void testPortfoliaoName() {
		when(portfolioRepository.findPortfolioByName("Keerthi")).thenReturn(portfolio);
		Portfolio result1 = portfolioServiceImpl.convertPortfolioToPortfolioResponseDto(portfolio);
		assertEquals(portfolio, result1);
	}

	@Test
	@DisplayName("Find by name:Negative Senario")
	public void testPortfoliaoName1() throws PortfolioNotFoundException {
		when(portfolioRepository.findPortfolioByName("sai")).thenReturn("No portfolios with this name");
		Portfolio result1 = portfolioServiceImpl.findByName("sai");
		assertThat(result1).isNotNull();
		assertEquals("No portfolios with this name", result1);
	}

}
