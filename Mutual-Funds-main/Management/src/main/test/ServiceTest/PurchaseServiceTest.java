package ServiceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.hcl.management.dto.PortfolioResponseDto;
import com.hcl.management.dto.PurchaseRequestDto;
import com.hcl.management.exception.ManagementException;
import com.hcl.management.exception.PortfolioNotFoundException;
import com.hcl.management.feignclient.PortfolioConnect;
import com.hcl.management.model.Account;
import com.hcl.management.model.Purchase;
import com.hcl.management.repository.AccountRepository;
import com.hcl.management.service.impl.PurchaseServiceImpl;
import com.hcl.portfolio.model.Portfolio;
import com.hcl.portfolio.repository.PortfolioRepository;
import com.hcl.portfolio.service.PortfolioServiceImpl;
@ExtendWith(MockitoExtension.class)

public class PurchaseServiceTest {

	@Mock
	AccountRepository accountRepository;
	@InjectMocks
	PurchaseServiceImpl purchaseServiceImpl;
	static Purchase purchase;
	static PurchaseRequestDto purchaseRequestDto;
	static PortfolioResponseDto portfolioResponseDto;
	static Account account;
	static PortfolioConnect portfolioConnect;
	
	
	
@Test
@DisplayName("Purchase: positive scenerio")
public void purchase() throws PortfolioNotFoundException, ManagementException {
	when(accountRepository.getById(purchaseRequestDto.getAccountId())).thenReturn(account);
	when(portfolioConnect.findById(purchaseRequestDto.getPortfolioId())).thenReturn(portfolioResponseDto);
	// when(purchaseRepository.save(purchase)).thenReturn(purchase);
	assertEquals("Purchased Successfully", purchaseServiceImpl.purchase(purchaseRequestDto));
}
@Test
@DisplayName("Purchase: negative scenerio")
public void purchase1() throws PortfolioNotFoundException, ManagementException {
	when(accountRepository.getById(purchaseRequestDto.getAccountId())).thenReturn(null);
	// when(portfolioConnect.findById(purchaseRequestDto.getPortfolioId())).thenReturn(portfolioResponseDto);
	// when(purchaseRepository.save(purchase)).thenReturn(purchase);
	assertThrows(ManagementException.class, () -> purchaseServiceImpl.purchase(purchaseRequestDto));
}
}