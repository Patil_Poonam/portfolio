package ControllerTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import com.hcl.management.dto.PurchaseRequestDto;
import com.hcl.management.exception.ManagementException;
import com.hcl.management.exception.PortfolioNotFoundException;
import com.hcl.management.service.impl.PurchaseServiceImpl;
import com.hcl.user.controller.UserController;
import com.hcl.user.dto.Credentials;
import com.hcl.user.service.IUserService;

@ExtendWith(MockitoExtension.class)
public class PurchaseControllerTest {
	@ModCheck
	PurchaseServiceImpl purchaseServiceImpl;
	@InjectMocks
	UserController userController;
	static Credentials credentials;
	static PurchaseRequestDto purchaseRequestDto;

	@Test
	@DisplayName("Portfolios Purchase:positive Scenario")
	public void purchaseTest() throws ManagementException, PortfolioNotFoundException {​​​​​​​​
	when(purchaseServiceImpl.purchase(purchaseRequestDto)).thenReturn("Purchased Successfully");
	String result1 = purchaseServiceImpl.purchase(purchaseRequestDto);
	assertEquals("Purchased Successfully", result1);


	}​​​​​​​​


	@Test
	@DisplayName("Portfolios Purchase:Negative Scenario")
	public void purchaseTestN() throws ManagementException, PortfolioNotFoundException {​​​​​​​​
	when(purchaseServiceImpl.purchase(purchaseRequestDto)).thenThrow(PortfolioNotFoundException.class);
	assertThrows(PortfolioNotFoundException.class, () -> purchaseController.purchase(purchaseRequestDto));


	}​​​​​​​​





}
